Stream.of(
Block.createCuboidShape(0, 0, 0, 16, 2, 16),
Block.createCuboidShape(4, 6, 7, 7, 15, 9),
Block.createCuboidShape(7, 6, 7, 9, 9, 9),
Block.createCuboidShape(9, 6, 7, 12, 15, 9),
Block.createCuboidShape(12.5, 3, 6.5, 15.5, 11, 9.5),
Block.createCuboidShape(13, 2, 7, 15, 13, 9),
Block.createCuboidShape(12, 11.5, 7.5, 16, 12.5, 8.5),
Block.createCuboidShape(0.5, 3, 6.5, 3.5, 11, 9.5),
Block.createCuboidShape(1, 2, 7, 3, 13, 9),
Block.createCuboidShape(0, 11.5, 7.5, 4, 12.5, 8.5)
).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, BooleanBiFunction.OR)).get();