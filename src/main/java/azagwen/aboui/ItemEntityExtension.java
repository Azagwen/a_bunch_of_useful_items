package azagwen.aboui;

import azagwen.aboui.magnet.MagneticElement;

public interface ItemEntityExtension {

    void addPossibleTarget(MagneticElement element);
}
