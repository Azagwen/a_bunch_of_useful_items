package azagwen.aboui;

import azagwen.aboui.magnet.gounded_electro_magnet.ElectroMagnetBlockEntity;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.registry.Registry;

public class BlockEntityRegistry {
    public static int BLOCK_ENTITY_COUNT = 0;
    public static final BlockEntityType<ElectroMagnetBlockEntity> GROUNDED_ITEM_CAPTIVATOR_ENTITY = registerBlockEntity("grounded_item_captivator", ElectroMagnetBlockEntity::new, BlockRegistry.GROUNDED_ITEM_CAPTIVATOR);

    public static <T extends BlockEntity> BlockEntityType<T> registerBlockEntity(String name, FabricBlockEntityTypeBuilder.Factory<T> factory, Block... blocks) {
        return Registry.register(Registry.BLOCK_ENTITY_TYPE, Aboui.id(name), FabricBlockEntityTypeBuilder.create(factory, blocks).build(null));
    }

    public static void init() {
        Aboui.LOGGER.info(String.format("%s Block Entit%S registered for %s!", BLOCK_ENTITY_COUNT, (BLOCK_ENTITY_COUNT > 1 ? "es" : "y"), Aboui.MOD_ID));
    }
}
