package azagwen.aboui;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.function.BiFunction;

public class BlockRegistrar {
    private final Identifier identifier;
    private final Block block;
    private BlockItem item;

    private BlockRegistrar(String name, Block block) {
        this(Aboui.id(name), block);
    }

    private BlockRegistrar(Identifier identifier, Block block) {
        this.identifier = identifier;
        this.block = block;
    }

    public static BlockRegistrar of(String name, Block block) {
        return new BlockRegistrar(name, block);
    }

    public static BlockRegistrar of(Identifier identifier, Block block) {
        return new BlockRegistrar(identifier, block);
    }

    public BlockRegistrar addItem(BiFunction<Block, Item.Settings, ? extends BlockItem> blockItemFunc, Item.Settings settings) {
        this.item = blockItemFunc.apply(this.block, settings);
        return this;
    }
    public Block register() {
        var finalBlock = Registry.register(Registry.BLOCK, this.identifier, this.block);
        if (this.item != null) {
            Registry.register(Registry.ITEM, this.identifier, this.item);
        }
        return finalBlock;
    }
}
