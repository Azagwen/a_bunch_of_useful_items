package azagwen.aboui.magnet;

import azagwen.aboui.ItemRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ItemMovement {
    private final MagneticElement owner;
    private final World world;
    private final Vec3d position;
    private final int attractionDistance = 20; // in Blocks
    private boolean canAttract;

    public ItemMovement(MagneticElement owner, World world) {
        this.owner = owner;
        this.world = world;
        this.position = owner.getElementPos();
    }

    public void tickAttractionMovement() {
        var dist = this.attractionDistance * 2;
        var box = Box.of(this.position, dist, dist, dist);
        var itemsInRange = world.getEntitiesByType(EntityType.ITEM, box, Entity::isAlive);

        for (var item : itemsInRange) {
            if (this.isOwnerValid(item)) {
                this.attractItem(item);
            }
        }
    }

    private void attractItem(ItemEntity item) {
        item.setNoGravity(true);
        // Create a vector "between" this item and the nearest target
        var targetToThis = this.position.subtract(item.getPos());
        // Determine how far the target is
        var distance = targetToThis.lengthSquared();
        // If the player is within range, start moving this item towards them + create magnet particles
        if (distance < (this.attractionDistance * this.attractionDistance)) {

            if (distance < ((this.attractionDistance  - 1) * (this.attractionDistance - 1))){
                var lineVector = this.position.subtract(targetToThis.multiply(MathHelper.nextDouble(this.world.getRandom(), 0, 1)));
                this.world.addParticle(ParticleTypes.ELECTRIC_SPARK, lineVector.x, lineVector.y, lineVector.z, 0, 0, 0);
            }

            var previousMovement = item.getVelocity();
            double attractionSpeed = 1.0 - Math.sqrt(distance) / this.attractionDistance;
            item.setVelocity(previousMovement.add(targetToThis.normalize().multiply(attractionSpeed * attractionSpeed * 0.1)));
        }
    }

    private boolean isOwnerValid(ItemEntity item) {
        var doesTargetExist = this.owner.stillExists();
        var isTargetWithinDistance = this.owner.getElementPos().squaredDistanceTo(item.getPos()) < (this.attractionDistance * this.attractionDistance);
        var isTargetHoldingMagnet = this.owner.getHeldStack().isOf(ItemRegistry.POCKET_ITEM_CAPTIVATOR);

        // If one of the above conditions is not met, make target null
        return doesTargetExist || isTargetHoldingMagnet || isTargetWithinDistance;
    }
}
