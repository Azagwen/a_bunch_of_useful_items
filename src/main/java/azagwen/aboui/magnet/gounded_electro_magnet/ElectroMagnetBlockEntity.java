package azagwen.aboui.magnet.gounded_electro_magnet;

import azagwen.aboui.BlockEntityRegistry;
import azagwen.aboui.ItemRegistry;
import azagwen.aboui.magnet.ItemMovement;
import azagwen.aboui.magnet.MagneticElement;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ElectroMagnetBlockEntity extends BlockEntity implements MagneticElement {

    public ElectroMagnetBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityRegistry.GROUNDED_ITEM_CAPTIVATOR_ENTITY, pos, state);
    }

    public static void tick(World world, BlockPos pos, BlockState state, ElectroMagnetBlockEntity magnetBlock) {
        if (!world.isClient() && state.get(ElectroMagnetBlock.ENABLED)) {
            var itemMovement = new ItemMovement(magnetBlock, world);
            itemMovement.tickAttractionMovement();
        }
    }

        @Override
    public Vec3d getElementPos() {
        return Vec3d.of(this.getPos()).add(0.5, 0.5, 0.5);
    }

    @Override
    public boolean stillExists() {
        return !this.isRemoved();
    }

    @Override
    public ItemStack getHeldStack() {
        return this.getCachedState().get(ElectroMagnetBlock.ENABLED) ? ItemRegistry.POCKET_ITEM_CAPTIVATOR.getDefaultStack() : ItemStack.EMPTY;
    }
}
