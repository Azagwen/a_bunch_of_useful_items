package azagwen.aboui.magnet;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;

public interface MagneticElement {

    Vec3d getElementPos();
    boolean stillExists(); //Exclusive to Entities
    ItemStack getHeldStack(); //Exclusive to Entities
}
