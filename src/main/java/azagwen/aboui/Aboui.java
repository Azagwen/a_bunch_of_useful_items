package azagwen.aboui;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.impl.blockrenderlayer.BlockRenderLayerMapImpl;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.util.Identifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Aboui {
    public static final String MOD_ID = "aboui";
    public static final Logger LOGGER = LoggerFactory.getLogger("aboui");

    public static Identifier id(String path) {
        return new Identifier(MOD_ID, path);
    }

    public static void initMain() {
        BlockRegistry.init();
        BlockEntityRegistry.init();
        ItemRegistry.init();

        LOGGER.info("Aboui initilialized");
    }

    @Environment(EnvType.CLIENT)
    public static void initClient() {
        BlockRenderLayerMapImpl.INSTANCE.putBlock(BlockRegistry.GROUNDED_ITEM_CAPTIVATOR, RenderLayer.getCutout());
    }
}
