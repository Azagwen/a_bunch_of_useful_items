package azagwen.aboui;

import azagwen.aboui.magnet.gounded_electro_magnet.ElectroMagnetBlock;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;

public class BlockRegistry {
    public static int BLOCK_COUNT = 0;
    public static final Block GROUNDED_ITEM_CAPTIVATOR = new ElectroMagnetBlock(FabricBlockSettings.copyOf(Blocks.COPPER_BLOCK).requiresTool());

    public static void init() {
        BlockRegistrar.of("grounded_item_captivator", GROUNDED_ITEM_CAPTIVATOR).addItem(BlockItem::new, new Item.Settings().group(ItemGroup.REDSTONE)).register();

        Aboui.LOGGER.info(String.format("%s Block%S registered for %s!", BLOCK_COUNT, (BLOCK_COUNT > 1 ? "s" : ""), Aboui.MOD_ID));
    }
}
