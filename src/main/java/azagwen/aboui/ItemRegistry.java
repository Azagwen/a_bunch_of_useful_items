package azagwen.aboui;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;

public class ItemRegistry {
    public static int ITEM_COUNT = 0;
    public static final Item POCKET_ITEM_CAPTIVATOR = registerItem("pocket_item_captivator", new Item(new Item.Settings().rarity(Rarity.UNCOMMON).maxCount(1).group(ItemGroup.TOOLS)));

    public static Item registerItem(String name, Item item) {
        ITEM_COUNT++;
        return Registry.register(Registry.ITEM, Aboui.id(name), item);
    }

    public static void init() {
        Aboui.LOGGER.info(String.format("%s Item%S registered for %s!", ITEM_COUNT, (ITEM_COUNT > 1 ? "s" : ""), Aboui.MOD_ID));
    }
}
