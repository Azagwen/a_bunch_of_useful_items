package azagwen.aboui.mixin;

import azagwen.aboui.ItemRegistry;
import azagwen.aboui.magnet.MagneticElement;
import com.google.common.collect.*;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.*;

//TODO: Optimise this

@Mixin(ItemEntity.class)
public abstract class MakeItemEntityMagnetic extends Entity {
    private @Unique MagneticElement target;
    private final @Unique int attractionDistance = 20; // in Blocks

    @Inject(method = "tick", at = @At("TAIL"))
    private void applyMagnetEffect (CallbackInfo ci)
    {
        this.updateTarget();

        // If the target is lost, re-enable gravity
        if (this.target == null) {
            this.setNoGravity(false);
        }

        // Follow Entity Target
        if (this.target != null) {
            this.followTarget(this.target.getElementPos());
        }
    }

    @Inject(method = "tryMerge(Lnet/minecraft/entity/ItemEntity;)V", at = @At("TAIL"), cancellable = true)
    private void tryMerge(ItemEntity other, CallbackInfo ci) {
        if (this.target != null) {
            ci.cancel();
        }
    }

    private void followTarget(Vec3d pos) {
        this.setNoGravity(true);
        // Create a vector "between" this item and the nearest player (target)
        var targetToThis = pos.subtract(this.getPos());
        // Determine how far the player is
        var distance = targetToThis.lengthSquared();
        // If the player is within range, start moving this item towards them + create magnet particles
        if (distance < (this.attractionDistance * this.attractionDistance)) {

            if (distance < ((this.attractionDistance  - 1) * (this.attractionDistance - 1))){
                var lineVector = pos.subtract(targetToThis.multiply(MathHelper.nextDouble(this.random, 0, 1)));
                world.addParticle(ParticleTypes.ELECTRIC_SPARK, lineVector.x, lineVector.y, lineVector.z, 0, 0, 0);
            }

            double attractionSpeed = 1.0 - Math.sqrt(distance) / this.attractionDistance;
            this.setVelocity(this.getVelocity().add(targetToThis.normalize().multiply(attractionSpeed * attractionSpeed * 0.1)));
        }
    }

    private void updateTarget() {
        this.checkTarget();
        var magneticElements = Maps.<Double, MagneticElement>newTreeMap();

        // Get magnetic entities that are in range and add them to a map
        var entitiesInSquareRadius = this.world.getOtherEntities(this, Box.of(this.getPos(), this.attractionDistance * 2, this.attractionDistance * 2, this.attractionDistance * 2));
        entitiesInSquareRadius.forEach(entity -> {
            if (entity instanceof MagneticElement element && element.getHeldStack().isOf(ItemRegistry.POCKET_ITEM_CAPTIVATOR)) {
                magneticElements.put(this.getDistanceFromMagneticElement(element), element);
            }
        });

        // Get possible magnetic blocks that are in range and add them to a map
        this.getReachableMagneticBlocks().forEach((element) -> {
            magneticElements.put(this.getDistanceFromMagneticElement(element), element);
        });

        // Set the closest magnetic element as the Target
        if (!magneticElements.isEmpty()) {
            var closestElement = magneticElements.firstEntry();
            if (closestElement != null) {
                this.target = closestElement.getValue();
            }
        }
    }

    private void checkTarget() {
        if (this.target != null) {
            var doesTargetExist = this.target.stillExists();
            var isTargetWithinDistance = this.target.getElementPos().squaredDistanceTo(this.getPos()) < (this.attractionDistance * this.attractionDistance);
            var isTargetHoldingMagnet = this.target.getHeldStack().isOf(ItemRegistry.POCKET_ITEM_CAPTIVATOR);

            // If one of the above conditions is not met, make target null
            if (!doesTargetExist || !isTargetHoldingMagnet || !isTargetWithinDistance) {
                this.target = null;
            }
        }
    }

    private double getDistanceFromMagneticElement(MagneticElement entity) {
        return entity != null ? entity.getElementPos().subtract(this.getPos()).lengthSquared() : Double.MAX_VALUE;
    }

    private List<MagneticElement> getReachableMagneticBlocks() {
        var vec = new Vec3i(this.attractionDistance, this.attractionDistance, this.attractionDistance);
        var blockPosIterable = BlockPos.iterate(this.getBlockPos().subtract(vec), this.getBlockPos().add(vec));
        var electroMagnets = Lists.<MagneticElement>newArrayList();

        blockPosIterable.forEach((pos) -> {
            if (this.world.getBlockEntity(pos) != null && this.world.getBlockEntity(pos) instanceof MagneticElement element && element.getHeldStack().isOf(ItemRegistry.POCKET_ITEM_CAPTIVATOR)) {
                electroMagnets.add(element);
            }
        });

        return electroMagnets;
    }

    public MakeItemEntityMagnetic(EntityType<?> type, World world) {
        super(type, world);
    }
}
