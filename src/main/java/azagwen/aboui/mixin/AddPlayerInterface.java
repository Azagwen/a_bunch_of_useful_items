package azagwen.aboui.mixin;

import azagwen.aboui.magnet.MagneticElement;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(PlayerEntity.class)
public abstract class AddPlayerInterface extends LivingEntity implements MagneticElement {

    protected AddPlayerInterface(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
    }

    @Override
    public Vec3d getElementPos() {
        return this.getPos().add(0, this.getStandingEyeHeight() / 2.0, 0);
    }

    @Override
    public boolean stillExists() {
        return !this.isSpectator() && this.isAlive();
    }

    @Override
    public ItemStack getHeldStack() {
        return this.getMainHandStack();
    }
}
