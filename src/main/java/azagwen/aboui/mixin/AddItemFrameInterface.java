package azagwen.aboui.mixin;

import azagwen.aboui.magnet.MagneticElement;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.decoration.GlowItemFrameEntity;
import net.minecraft.entity.decoration.ItemFrameEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

public class AddItemFrameInterface{

    @Mixin(ItemFrameEntity.class)
    public abstract static class ItemFrameMixin extends Entity implements MagneticElement {

        @Shadow
        public abstract ItemStack getHeldItemStack();

        protected ItemFrameMixin(EntityType<? extends LivingEntity> entityType, World world) {
            super(entityType, world);
        }

        @Override
        public Vec3d getElementPos() {
            return this.getPos();
        }

        @Override
        public boolean stillExists() {
            return !this.isSpectator() && this.isAlive();
        }

        @Override
        public ItemStack getHeldStack() {
            return this.getHeldItemStack();
        }

    }

    @Mixin(GlowItemFrameEntity.class)
    public abstract static class GlowItemFrameMixin extends ItemFrameEntity implements MagneticElement {

        protected GlowItemFrameMixin(EntityType<? extends ItemFrameEntity> entityType, World world) {
            super(entityType, world);
        }

        @Override
        public Vec3d getElementPos() {
            return this.getPos();
        }

        @Override
        public boolean stillExists() {
            return !this.isSpectator() && this.isAlive();
        }

        @Override
        public ItemStack getHeldStack() {
            return this.getHeldItemStack();
        }
    }
}